﻿using System.Collections.Generic;
using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using UnityEngine;
using System;

namespace FlowEngine.UnitTests {
    public class PromiseTests {
        
        [Test]
        public void CanExecuteResolvedHandler() {
            Promise.Resolved()
                .Then(
                    ()=> Assert.Pass(),
                    e => Assert.Fail()
                );
        }

        [Test]
        public void CanExecuteExceptionHandler() {
            Promise.Rejected(new Exception())
                .Then(
                    ()=> Assert.Fail(),
                    e => Assert.Pass()
                );
        }

        [Test]
        public void CanPassDownValues() {
            var promise = new Promise<int>() ;
            
            promise
                .Then(null)
                .Then(null)
                .Then(val => Assert.IsTrue(val == promise.Value))
                .Catch(e  => Assert.Fail());
            
            promise.Resolve(42);
        }

        [Test]
        public void CanPassDownExceptions() {
            var promise = new Promise() ;
            
            promise
                .Then(null)
                .Then(null)
                .Then(() => Assert.Fail())
                .Catch(e => Assert.IsTrue(e == promise.RejectionException));
            
            promise.Reject(new Exception());
        }

        [Test]
        public void CannotChangeStateAfterResolving() {
            var promise = new Promise() ;
            promise.Resolve();

            var exc1 = Assert.Catch(() => promise.Resolve());
            var exc2 = Assert.Catch(() => promise.Reject(new Exception()));
            Assert.False(exc1 == null || exc2 == null);
        }

        [Test]
        public void CannotChangeStateAfterRejecting() {
            var promise = new Promise() ;
            promise.Reject(new Exception());

            var exc1 = Assert.Catch(() => promise.Resolve());
            var exc2 = Assert.Catch(() => promise.Reject(new Exception()));
            Assert.False(exc1 == null || exc2 == null);
        }

        [Test]
        public void WillCallNewHandlersAfterResolving() {
            var promise = Promise<int>.Resolved(42);
            int result = 0 ;

            promise.Then(val => { result = val ; });
            Assert.True(result == promise.Value);
        }

        [Test]
        public void WillCallNewHandlersAfterRejecting() {
            var promise = Promise<int>.Rejected(new Exception());
            Exception result = null ;

            promise.Catch(e => { result = e ; });
            Assert.True(result == promise.RejectionException);
        }

        [Test]
        public void WillNotCallErrorHandlersAfterResolving() {
            var promise = Promise<int>.Resolved(42);
            promise.Then(() => Assert.Fail());
            promise.Catch(e => Assert.Pass());
        }

        [Test]
        public void WillNotCallResolutionHandlersAfterRejecting() {
            var promise = Promise<int>.Rejected(new Exception());
            promise.Then(() => Assert.Fail());
            promise.Catch(e => Assert.Pass());
        }

        [Test]
        public void FinallyIsCalledAfterResolve() {
            Promise.Resolved()
                .Then(() => Assert.Fail())
                .Finally(() => Assert.Pass());
        }

        [Test]
        public void FinallyIsCalledAfterRejection() {
            Promise.Rejected(new Exception())
                .Catch(e => Assert.Fail())
                .Finally(() => Assert.Pass());
        }

        [Test]
        public void CanResolveCombinedPromise() {
            Promise.All(new [] {
                Promise.Resolved(),
                Promise.Resolved(),
                Promise.Resolved()
            })
            .Then(() => Assert.Pass())
            .Finally(() => Assert.Fail());
        }

        [Test]
        public void CanResolveCombinedGenericPromise() {
            Promise<int>.All(new [] {
                Promise<int>.Resolved(1),
                Promise<int>.Resolved(2),
                Promise<int>.Resolved(3)
            })
            .Then(arr => Assert.True(
                arr.Length == 3 
                && arr[0] == 1 
                && arr[0] == 2 
                && arr[0] == 3
            ))
            .Finally(() => Assert.Fail());
        }

        [Test]
        public void CanCatchRejectedCombinedPromise() {
            Promise.All(new [] {
                Promise.Rejected(new Exception()),
                Promise.Resolved(),
                Promise<int>.Resolved(3)
            })
            .Then(
                ()=> Assert.Fail(),
                e => Assert.Pass()
            );
        }

        [Test]
        public void CanCatchRejectedCombinedGenericPromise() {
            Promise<int>.All(
                Promise<int>.Rejected(new Exception()),
                Promise<int>.Resolved(2),
                Promise<int>.Resolved(3)
            )
            .Then(
                a => { Assert.Fail(); },
                e => { Assert.Pass(); }
            );
        }

        [Test]
        public void CanResolveRaceCondition() {
            Promise.Race(new [] {
                Promise.Resolved(),
                Promise.Resolved(),
                Promise.Resolved()
            })
            .Then(() => Assert.Pass())
            .Finally(() => Assert.Fail());
        }

        [Test]
        public void CanResolveGenericRaceCondition() {
            Promise<int>.Race(new [] {
                Promise<int>.Resolved(1),
                Promise<int>.Resolved(2),
                Promise<int>.Resolved(3)
            })
            .Then(v => Assert.True(v == 1))
            .Finally(() => Assert.Fail());
        }

        [Test]
        public void CanRejectRaceCondition() {
            Promise.Race(new [] {
                Promise.Rejected(new Exception()),
                Promise.Resolved(),
                Promise.Resolved()
            })
            .Then(() => Assert.Fail())
            .Catch(e => Assert.Pass());
        }

        [Test]
        public void CanRejectGenericRaceCondition() {
            Promise<int>.Race(new [] {
                Promise<int>.Rejected(new Exception()),
                Promise<int>.Resolved(2),
                Promise<int>.Resolved(3)
            })
            .Then(v => Assert.Fail())
            .Catch(e => Assert.Pass());
        }
        
    }
}
