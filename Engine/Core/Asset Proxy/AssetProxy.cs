using UnityEngine ;

namespace FlowEngine {

  public abstract class AssetProxy : MonoBehaviour {
    [SerializeField] protected Object target ;

    public Object Asset {
      get { return target ; }
      set { target = value ; }
    }

  }
}
