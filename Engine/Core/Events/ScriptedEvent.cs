using System.Collections.Generic ;
using System ;

using UnityEngine ;


namespace FlowEngine {

  [System.Serializable, CreateAssetMenuAttribute(menuName = "Flow/Event", fileName = "Event")]
  public class ScriptedEvent : ScriptableObject, ISubscribable, ISubscribableGeneric {


    public void RemoveListener (Action deleg) {
      BroadCaster<ScriptedEvent>.RemoveListener (this, deleg) ;
    }

    public void AddListener (Action deleg) {
      BroadCaster<ScriptedEvent>.AddListener (this, deleg) ;
    }

    public void Raise () {
      BroadCaster<ScriptedEvent>.Cast (this) ;
    }


    public void RemoveListener<T> (Action<T> deleg) {
      BroadCaster<ScriptedEvent, T>.RemoveListener (this, deleg) ;
    }

    public void AddListener<T> (Action<T> deleg) {
      BroadCaster<ScriptedEvent, T>.AddListener (this, deleg) ;
    }

    public void Raise<T> (T parameter) {
      BroadCaster<ScriptedEvent, T>.Cast (this, parameter) ;
    }

  }
}
