using System.Collections.Generic ;
using System ;


namespace FlowEngine {

  public static class BroadCaster<TMessage, TParam> {

    private static Dictionary<TMessage, Action<TParam>> delegates ;

    static BroadCaster () {
      delegates = new Dictionary<TMessage, Action<TParam>> () ;
    }

    /* RemoveListener
     * TMessage message, Action<TParam> deleg
     *
     * Desc.
     *
     *
     */
    public static void RemoveListener (TMessage message, Action<TParam> deleg) {
      if(delegates.ContainsKey(message))
        delegates[message] -= deleg ;
    }

    /* AddListener
     * TMessage message, Action<TParam> deleg
     *
     * Desc.
     *
     *
     */
    public static void AddListener (TMessage message, Action<TParam> deleg) {
      if(!delegates.ContainsKey(message))
        delegates[message] = p=>{} ;
      delegates[message] += deleg ;
    }

    /* Cast
     * TMessage message, TParam parameter
     *
     * Desc.
     *
     *
     */
    public static void Cast (TMessage message, TParam parameter) {
      if(delegates.ContainsKey(message))
        delegates[message] (parameter) ;
    }

  }
}
