using System.Collections.Generic ;
using System ;


namespace FlowEngine {

  public static class BroadCaster<TMessage> {

    private static Dictionary<TMessage, Action> delegates ;

    static BroadCaster () {
      delegates = new Dictionary<TMessage, Action> () ;
    }

    /* RemoveListener
     * TMessage message, Action deleg
     *
     * Desc.
     *
     *
     */
    public static void RemoveListener (TMessage message, Action deleg) {
      if(delegates.ContainsKey(message))
        delegates[message] -= deleg ;
    }

    /* AddListener
     * TMessage message, Action deleg
     *
     * Desc.
     *
     *
     */
    public static void AddListener (TMessage message, Action deleg) {
      if(!delegates.ContainsKey(message))
        delegates[message] = ()=>{} ;
      delegates[message] += deleg ;
    }

    /* Cast
     * TMessage message
     *
     * Desc.
     *
     *
     */
    public static void Cast (TMessage message) {
      if(delegates.ContainsKey(message))
        delegates[message] () ;
    }

  }
}
