using UnityEngine;
using System;

// ////////////////////// //
//  Niet door mij gemaakt
// ////////////////////// //

namespace FlowEngine {

	[Serializable]
	public sealed class SerializableType : ISerializationCallbackReceiver {

    [SerializeField] private string _typeRef;
		private Type _type;

		public Type Type {
			get { return _type; }
			set {
				if (value != null && !value.IsClass)
					throw new ArgumentException(string.Format("'{0}' is not a class type.", value.FullName), "value") ;
				_type = value;
				_typeRef = GetClassRef(value);
			}
		}

		public SerializableType(string assemblyQualifiedClassName) {
			Type = !string.IsNullOrEmpty(assemblyQualifiedClassName)
				? Type.GetType(assemblyQualifiedClassName)
				: null;
		}

		public SerializableType(Type type) {
			Type = type;
		}

    public SerializableType() {}




		public static implicit operator string(SerializableType typeReference) {
			return typeReference._typeRef;
		}

		public static implicit operator Type(SerializableType typeReference) {
			return typeReference.Type;
		}

		public static implicit operator SerializableType(Type type) {
			return new SerializableType(type);
		}

		public override string ToString() {
			return Type != null ? Type.FullName : "(None)";
		}

    // //////////////////////////////// //
    //  ISerializationCallbackReceiver
    // //////////////////////////////// //
		void ISerializationCallbackReceiver.OnAfterDeserialize() {
			if (!string.IsNullOrEmpty(_typeRef)) {
				_type = System.Type.GetType(_typeRef);

				if (_type == null)
					Debug.LogWarning(string.Format("'{0}' was referenced but class type was not found.", _typeRef));
			}
			else
				_type = null;
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize() {
		}

    // ///////// //
    //  Statics
    // ///////// //
		public static string GetClassRef(Type type) {
			return type != null
				? type.FullName + ", " + type.Assembly.GetName().Name
				: "";
		}

	}

}
