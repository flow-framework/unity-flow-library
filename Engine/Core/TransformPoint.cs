using UnityEngine ;

namespace FlowEngine {

  [System.Serializable]
  public struct TransformPoint {

    public Quaternion rotation ;
    public Vector3 position ;

    public TransformPoint(Vector3 position, Quaternion rotation) {
      this.rotation = rotation ;
      this.position = position ;
    }

    public TransformPoint ApplyTransformation (Transform t) {
      return new TransformPoint (
        t.TransformPoint(this.position),
        this.rotation
      ) ;
    }

    public TransformPoint InverseTransformation (Transform t) {
      return new TransformPoint (
        t.InverseTransformPoint(this.position),
        this.rotation
      ) ;
    }

  }
}
