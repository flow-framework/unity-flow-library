using System.Collections.Generic;
using System.Linq;
using System ;


namespace FlowEngine {
    public class Promise : IPromise {

        //
        private const string EX_FORMAT_NOT_PENDING = "Unable to {1} Promise that was {0} prior." ;

        //
        private Action<Exception> RejectedHandlers ;
        private Action ResolvedHandlers ;

        /// <summary>
        /// Current state of the Promise.
        /// </summary>
        public PromiseState State { get; private set ; }

        /// <summary>
        /// Error the promise encountered, null by default.
        /// </summary>
        public Exception RejectionException { get; private set; }


        // Constructors
        public Promise () {
            State = PromiseState.PENDING ;
        }

        public Promise (Action<Action, Action<Exception>> resolver) {
            State = PromiseState.PENDING ;

            try { 
                resolver(Resolve, Reject);
            }
            catch( Exception e ) {
                Reject(e);
            }
        }

        // Handlers
        private void AddRejectedHandler(Action<Exception> handler) {
            if(State == PromiseState.PENDING)
                RejectedHandlers += handler ;
            
            // Call handler immediately if the promise was already rejected.
            else if(State == PromiseState.REJECTED)
                handler(RejectionException) ;
        }

        private void AddResolvedHandler(Action handler) {
            if(State == PromiseState.PENDING)
                ResolvedHandlers += handler ;
            
            // Call handler immediately if the promise already resolved.
            else if(State == PromiseState.RESOLVED)
                handler() ;
        }

        private void ClearHandlers() {
            RejectedHandlers = null ;
            ResolvedHandlers = null ;
        }
        
        // IRejectable
        public void Reject(Exception e) {
            if(State != PromiseState.PENDING)
                throw new Exception(String.Format(EX_FORMAT_NOT_PENDING, State, "REJECT")) ;

            State = PromiseState.REJECTED ;
            RejectionException = e ;
            
            (RejectedHandlers ?? (ex=>{}))(e) ;
            ClearHandlers() ;
        }


        // IResolvable
        public void Resolve() {
            if(State != PromiseState.PENDING)
                throw new Exception(String.Format(EX_FORMAT_NOT_PENDING, State, "RESOLVE")) ;

            State = PromiseState.RESOLVED ;

            (ResolvedHandlers ?? (()=>{}))() ;
            ClearHandlers() ;
        }

        /// IPromise
        /// <summary>
        /// () => TargetT
        /// </summary>
        public IPromise<TargetT> Then<TargetT>(Func<IPromise<TargetT>> onResolved, Func<Exception, IPromise<TargetT>> onRejected) {
            return CreateThenable<Promise<TargetT>> (
                CreateHandler<TargetT>(onResolved),
                CreateRejectionHandler<TargetT>(onRejected)
            );
        }
        
        /// <summary>
        /// () => TargetT
        /// </summary>
        public IPromise<TargetT> Then<TargetT>(Func<IPromise<TargetT>> onResolved, Action<Exception> onRejected = null) {
            return CreateThenable<Promise<TargetT>> (
                CreateHandler<TargetT>(onResolved),
                CreateRejectionHandler(onRejected)
            );
        }

        /// <summary>
        /// () => {}
        /// </summary>
        public IPromise Then(Action onResolved, Action<Exception> onRejected = null) {
            return CreateThenable<Promise> (
                CreateHandler(onResolved),
                CreateRejectionHandler(onRejected)
            );
        }
        

        // Finally
        public IPromise Finally(Action onComplete) {
            return new Promise();
        }
        
        // Catch
        public IPromise Catch(Action<Exception> onRejected) {
            return CreateThenable<Promise>(
                resultPromise => resultPromise.Resolve(),
                CreateRejectionHandler(onRejected)
            );
        }

        /* ! Factory Method to prevent duplicate code ! */
        protected ThenableT CreateThenable<ThenableT>(Action<ThenableT> resolvableHandler, Action<ThenableT, Exception> rejectionHandler) where ThenableT : IPromise, new() {
            var target = new ThenableT() ;

            AddResolvedHandler(() => {
                // Wrap resolve handler
                try                 { resolvableHandler(target) ; }
                catch(Exception ex) { target.Reject(ex) ; }
            }) ;
        
            AddRejectedHandler((e) => {
                // Wrap reject handler
                try                 { rejectionHandler(target, e) ; }
                catch(Exception ex) { target.Reject(ex) ; }
            });

            return target ;
        }

        // Out
        protected Action<IPromise<OutT>> CreateHandler<OutT>(Func<IPromise<OutT>> handler) {
            if(handler == null)
                return resultPromise => resultPromise.Resolve() ;

            return resultPromise => {
                handler().Then(
                    val => resultPromise.Resolve(val),
                    exc => resultPromise.Reject(exc)
                );
            };
        }

        // Neither
        protected Action<IPromise> CreateHandler(Action handler) {
            if(handler == null)
                return resultPromise => resultPromise.Resolve() ;

            return resultPromise => {
                handler() ;
                resultPromise.Resolve() ;
            };
        }

        // Exception with output
        protected Action<IPromise<OutT>, Exception> CreateRejectionHandler<OutT>(Func<Exception, IPromise<OutT>> handler) {
            if(handler == null)
                return (resultPromise, e) => resultPromise.Reject(e) ;

            return (resultPromise, e) => {
                handler(e).Then(
                    val => resultPromise.Resolve(val),
                    exc => resultPromise.Reject(exc)
                );
            };
        }

        // Exception
        protected Action<IPromise, Exception> CreateRejectionHandler(Action<Exception> handler) {
            if(handler == null)
                return (resultPromise, e) => resultPromise.Reject(e) ;

            return (resultPromise, e) => {
                handler(e) ;
                resultPromise.Resolve() ;
            };
        }

        /// <summary>
        /// Returns a resolved Promise without value.
        /// </summary>
        public static Promise Resolved() {
            var p = new Promise() ;
            p.State = PromiseState.RESOLVED ;
            return p ;
        }

        /// <summary>
        /// Returns a rejected Promise without value.
        /// </summary>
        public static Promise Rejected(Exception e) {
            var p = new Promise() ;
            p.Reject(e) ;
            return p ;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Promise Race(params IPromise[] promises) {
            return Race((IEnumerable<IPromise>)promises) ;
        }

        /// <summary>
        /// Returns a promise that resolves or rejects as soon as one of the promises in an 
        /// iterable resolves or rejects, with the reason from that promise.
        /// </summary>
        public static Promise Race(IEnumerable<IPromise> promises) {
            var racers = promises.ToArray() ;

            if(racers.Length == 0)
                return Promise.Resolved() ;

            var resultPromise = new Promise();

            foreach(var promise in racers)
                promise.Then(
                    ()=>{
                        if(resultPromise.State == PromiseState.PENDING)
                            resultPromise.Resolve();
                    },
                    e =>{
                        if(resultPromise.State == PromiseState.PENDING)
                            resultPromise.Reject(e);
                    }
                );

            
            return resultPromise ;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Promise All(params IPromise[] promises) {
            return All((IEnumerable<IPromise>) promises) ;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Promise All(IEnumerable<IPromise> promises) {
            var racers = promises.ToArray() ;

            if(racers.Length == 0)
                return Promise.Resolved() ;

            var runningPromises = racers.Length;
            var resultPromise = new Promise();

            for(var i = 0; i < racers.Length ; ++ i)
                racers[i]
                    .Catch(e =>{
                        if(resultPromise.State == PromiseState.PENDING)
                            resultPromise.Reject(e);
                    })
                    .Finally(()=>{
                        -- runningPromises;

                        if(runningPromises == 0 && resultPromise.State == PromiseState.PENDING)
                           resultPromise.Resolve(); 
                    });

            
            return resultPromise ;
        }
        
    }
}

