using System.Collections.Generic;
using System.Linq;
using System;

namespace FlowEngine {
    public class Promise<PromisedT> : Promise, IPromise<PromisedT> {

        public PromisedT Value { get; private set ; }

        public Promise () : base () {
            // Calls the inherited constructor
        }
        
        public Promise ( Action<Action<PromisedT>, Action<Exception>> resolver ) : base() {
            try { 
                resolver(Resolve, Reject);
            }
            catch( Exception e ) {
                Reject(e);
            }
        }

        

        public void Resolve(PromisedT value) {
            if(State == PromiseState.PENDING)
                Value = value ;
            Resolve() ;
        }

        public IPromise<PromisedT> Catch(Func<Exception, IPromise<PromisedT>> onRejected){
            return CreateThenable<Promise<PromisedT>> (
                resultPromise => resultPromise.Resolve(Value),
                CreateRejectionHandler<PromisedT>(onRejected)
            );
        }

        public new IPromise<PromisedT> Catch(Action<Exception> onRejected) {
            return CreateThenable<Promise<PromisedT>> (
                resultPromise => resultPromise.Resolve(Value),
                CreateRejectionHandler(onRejected)
            );
        }

        /* Then
         * val=>val2
         */
        public IPromise<TargetT> Then<TargetT>(Func<PromisedT, IPromise<TargetT>> onResolved, Func<Exception, IPromise<TargetT>> onRejected) {
            return CreateThenable<Promise<TargetT>> (
                CreateHandler<TargetT>(onResolved),
                CreateRejectionHandler<TargetT>(onRejected)
            );
        }

        public IPromise<TargetT> Then<TargetT>(Func<PromisedT, IPromise<TargetT>> onResolved, Action<Exception> onRejected = null) {
            return CreateThenable<Promise<TargetT>> (
                CreateHandler<TargetT>(onResolved),
                CreateRejectionHandler(onRejected)
            );
        }

        /* Then
         * val=>val
         */
        public IPromise<PromisedT> Then(Func<PromisedT, IPromise<PromisedT>> onResolved, Func<Exception, IPromise<PromisedT>> onRejected) {
            return CreateThenable<Promise<PromisedT>> (
                CreateHandler<PromisedT>(onResolved),
                CreateRejectionHandler<PromisedT>(onRejected)
            );
        }

        public IPromise<PromisedT> Then(Func<PromisedT, IPromise<PromisedT>> onResolved, Action<Exception> onRejected = null) {
            return CreateThenable<Promise<PromisedT>> (
                CreateHandler<PromisedT>(onResolved),
                CreateRejectionHandler(onRejected)
            );
        }

        /* Then
         * val=>{}
         */
        public IPromise Then(Action<PromisedT> onResolved, Action<Exception> onRejected = null) {
            return CreateThenable<Promise<PromisedT>> (
                CreateHandler(onResolved),
                CreateRejectionHandler(onRejected)
            );
        }

        /* ! Factory Methods to prevent duplicate code ! */
        // In Out
        protected Action<IPromise<OutT>> CreateHandler<OutT>(Func<PromisedT, IPromise<OutT>> handler) {
            if(handler == null)
                return resultPromise => resultPromise.Resolve() ;

            return resultPromise => {
                handler( Value ).Then(
                    val => resultPromise.Resolve(val),
                    exc => resultPromise.Reject(exc)
                );
            };
        }

        // In
        protected Action<IPromise> CreateHandler(Action<PromisedT> handler) {
            if(handler == null)
                return resultPromise => resultPromise.Resolve() ;
                
            return resultPromise => {
                handler(Value) ;
                resultPromise.Resolve() ;
            };
        }

                /// <summary>
        /// Returns a resolved Generic Promise with value (T value).
        /// </summary>
        public static Promise<PromisedT> Resolved(PromisedT value) {
            var p = new Promise<PromisedT>() ;
            p.Resolve(value);
            return p ;
        }

        /// <summary>
        /// Returns a rejected Generic Promise without value.
        /// </summary>
        new public static Promise<PromisedT> Rejected(Exception e) {
            var p = new Promise<PromisedT>() ;
            p.Reject(e) ;
            return p ;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Promise<PromisedT> Race(params IPromise<PromisedT>[] promises) {
            return Race((IEnumerable<IPromise<PromisedT>>)promises) ;
        }

        /// <summary>
        /// Returns a promise that resolves or rejects as soon as one of the promises in an 
        /// iterable resolves or rejects, with the value or reason from that promise.
        /// </summary>
        public static Promise<PromisedT> Race(IEnumerable<IPromise<PromisedT>> promises) {
            var racers = promises.ToArray() ;
            
            if(racers.Length == 0)
                return Promise<PromisedT>.Resolved(default(PromisedT)) ;

            var resultPromise = new Promise<PromisedT>();

            foreach(var promise in racers)
                promise.Then(
                    v =>{
                        if(resultPromise.State == PromiseState.PENDING)
                            resultPromise.Resolve(v);
                    },
                    e =>{
                        if(resultPromise.State == PromiseState.PENDING)
                            resultPromise.Reject(e);
                    }
                );
            
            return resultPromise ;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Promise<PromisedT[]> All(params IPromise<PromisedT>[] promises) {
            return All((IEnumerable<IPromise<PromisedT>>)promises) ;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Promise<PromisedT[]> All(IEnumerable<IPromise<PromisedT>> promises) {
            var racers = promises.ToArray() ;           

            if(racers.Length == 0)
                return Promise<PromisedT[]>.Resolved(new PromisedT[0]) ;

            var runningPromises = racers.Length ;
            var resultPromise = new Promise<PromisedT[]>();
            var results = new PromisedT[runningPromises] ;

            for(var i = 0; i < racers.Length ; ++ i)
                racers[i].Then(
                    val =>{
                        -- runningPromises;
                        results[ i ] = val ;

                        if(runningPromises == 0 && resultPromise.State == PromiseState.PENDING)
                           resultPromise.Resolve(results); 
                           
                    },
                    e => {
                        if(resultPromise.State == PromiseState.PENDING)
                            resultPromise.Reject(e);
                    }
                );

            
            return resultPromise ;
        }
        
    }
}