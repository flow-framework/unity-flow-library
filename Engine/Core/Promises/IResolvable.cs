namespace FlowEngine {
    public interface IResolvable {

        void Resolve() ;

    } 
}