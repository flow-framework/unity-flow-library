namespace FlowEngine {
    public enum PromiseState {
        PENDING,
        RESOLVED,
        REJECTED
    }
}