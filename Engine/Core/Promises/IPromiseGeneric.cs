using System ;

namespace FlowEngine {
    public interface IPromise< PromisedT > : IPromise, IResolvable< PromisedT > {

        PromisedT Value { get; }

        /// <summary>
        /// PromisedT => TargetT
        /// </summary>
        IPromise<TargetT> Then<TargetT>(Func<PromisedT, IPromise<TargetT>> onResolved, Func<Exception, IPromise<TargetT>> onRejected);
        IPromise<TargetT> Then<TargetT>(Func<PromisedT, IPromise<TargetT>> onResolved, Action<Exception> onRejected = null);

        /// <summary>
        /// Is this method necessary
        /// PromisedT => PromisedT
        /// </summary>
        IPromise<PromisedT> Then(Func<PromisedT, IPromise<PromisedT>> onResolved, Func<Exception, IPromise<PromisedT>> onRejected);
        IPromise<PromisedT> Then(Func<PromisedT, IPromise<PromisedT>> onResolved, Action<Exception> onRejected = null);

        // IPromise Then(Func<PromisedT, IPromise> onResolved, Action<Exception> onRejected = null);
        /// <summary>
        /// PromisedT => {}
        /// </summary>
        IPromise Then(Action<PromisedT> onResolved, Action<Exception> onRejected = null);

        /// <summary>
        /// Exception => PromisedT
        /// </summary>
        IPromise<PromisedT> Catch(Func<Exception, IPromise<PromisedT>> onRejected);

        /// <summary>
        /// Exception => PromisedT
        /// </summary>
        new IPromise<PromisedT> Catch(Action<Exception> onRejected);
    } 
}