using System ;

namespace FlowEngine {
    public interface IRejectable {

        void Reject( Exception e ) ;

    } 
}