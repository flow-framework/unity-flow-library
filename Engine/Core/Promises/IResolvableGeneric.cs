namespace FlowEngine {
    public interface IResolvable< ResolvedT > {

        void Resolve( ResolvedT value ) ;

    } 
}