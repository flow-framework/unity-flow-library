using System ;

namespace FlowEngine {
    public interface IPromise : IResolvable, IRejectable {

        /// <summary>
        /// Current state of the Promise.
        /// </summary>
        PromiseState State { get; }

        /// <summary>
        /// Error the promise encountered, null by default.
        /// </summary>
        Exception RejectionException { get; }

        /// <summary>
        /// () => TargetT
        /// </summary>
        IPromise<TargetT> Then<TargetT>(Func<IPromise<TargetT>> onResolved, Func<Exception, IPromise<TargetT>> onRejected);
        IPromise<TargetT> Then<TargetT>(Func<IPromise<TargetT>> onResolved, Action<Exception> onRejected = null);

        /// <summary>
        /// () => {}
        /// </summary>
        IPromise Then(Action onResolved, Action<Exception> onRejected = null);

        /// <summary>
        /// Exception => {}
        /// </summary>
        IPromise Catch(Action<Exception> onRejected);

        /// <summary>
        /// () => {}
        /// </summary>
        IPromise Finally(Action onComplete);

    } 
}