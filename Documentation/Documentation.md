
# Flow Library
The Unity Game Engine is build to be easily extensible, however this resulted in thousands of first and third party extensions each implementing their own workflow. The goal of the Flow Library is to deliver tools to quickly build extensions that follow a unified workflow, allowing less technical users to quickly learn how to use your extension without support from the programmer.

# Current state
Currently, the Flow Library attempts to enforce loose coupling wherever possible by moving data from the GameObjects to assets through _Asset stored variables_ and _events_.
_Proxies_ can be used to manipulate data from within the scene using purpose build tools and through interaction from other GameObjects within the game.

The _DataStores_ are meant to make it more easy to store persistent data, both in the editor and within the game.


# Usage
- [Proxies](#proxies)
- [Global Variables](#variables)
- [Events and Messages](#events-channels)
- [DataStores](#datastores)
- [Tools](#tools)
- [Scheduled Features](#more)


### Proxies  <a name="proxies"></a>
Proxies are components used to manipulate data in assets from within a scene.

The proxies can be created by inheriting from the generic _AssetProxy_ class.
When used in conjunction with the _TypeTargetAttribute_, assets of the specified type can be dragged from the project panel into the scene to create a new GameObject with the appropriate _AssetProxy_ component.


```c#
/* Example */
[FlowEngine.TypeTargetAttribute(typeof(SplineData))]
public class Spline : FlowEngine.AssetProxy<SplineData> {}
```

### Global Variables  <a name="variables"></a>
Global Variables are variables stored as assets.
These have been inspired by Ryan Hipple's talk from [Unite `17][707929ed]. <br>
New types can be created by inheriting from the generic _VariableContainer_ class.

```c#
/* Example */
[UnityEngine.CreateAssetMenuAttribute(menuName = "Flow/Variable/Float", fileName = "Float")]
[System.Serializable] public class FloatVariable : FlowEngine.VariableContainer <float> {}
```

The generic class _VariableContainerReference_ can be used to create a wrapper for the Global Variables which allows the user to either set a local variable or a global variable asset in the inspector.

```c#
/* Example */
[System.Serializable] public class FloatReference : FlowEngine.VariableContainerReference <float, FloatVariable> {}
```

A listener can be added to a Global Variable through the _AddListener(Action<T> deleg)_ method to listen for changes in the value.

### Events and Messages  <a name="events-channels"></a>
The Flow library allows for events through the static _BroadCaster_ class.
Objects of any type can be used as a Channel/Message identifiers through the first generic Type.
The _BroadCaster_ supports either 1 or 0 parameters of any type in the callback, by using the second generic Type parameter.

```c#
/* Example */
using UnityEngine;
using FlowEngine;

public class BroadCasterTest : MonoBehaviour {

  void Awake () {
    // Without parameter
    BroadCaster<string>.AddListener           ("Message", Callback) ;
    BroadCaster<string>.Cast                  ("Message") ;
    BroadCaster<string>.RemoveListener        ("Message", Callback) ;

    // With parameter
    BroadCaster<string, float>.AddListener    ("Message", Callback) ;
    BroadCaster<string, float>.Cast           ("Message", 42) ;
    BroadCaster<string, float>.RemoveListener ("Message", Callback) ;
  }

  void Callback (float a) {
    Debug.Log(a) ;
  }

  void Callback () {
    Debug.Log("The answer.") ;
  }

}
```

Events can also be created from the asset menu _"Flow/Event"_. These events are recommended as they don't use hardcoded strings or values as the message identifier and can be injected through the inspector, preventing tightly coupled references.

```c#
/* Example */
using UnityEngine;
using FlowEngine;

public class EventTest : MonoBehaviour {

  [UnityEngine.SerializeField]
  private ScriptedEvent e ;

  void Awake () {
    // Without parameter
    e.AddListener    (Callback) ;
    e.Raise          () ;
    e.RemoveListener (Callback) ;

    // With parameter
    e.AddListener<float>    (Callback) ;
    e.Raise<float>          (42) ;
    e.Raise<float>          (63) ;
    e.RemoveListener<float> (Callback) ;
  }

  void Callback (float a) {
    Debug.Log(a) ;
  }

  void Callback () {
    Debug.Log("The answer.") ;
  }

}
```


### DataStores <a name="datastores"></a>
DataStores allow the user to read and write instances inheriting class _DataStoreContainer_ to and from permanent storage, and can be created through the static method:
```c#
FlowEngine.DataStore.Load(string path) ;
```
and stored using static method:
```c#
FlowEngine.DataStore.Save (DataStore store) ;
```

By default the data is stored at path  ```"Application.persistentDataPath/path"```, however this can be overwritten by creating the store using the method:
```c#
FlowEngine.DataStore.LoadAtPath (string baseDir, string path) ;
```

DataStores can also be created using the asset menu _"Flow/Data Store"_.

**Preference Editor** <br>
The PreferenceEditor class is a special DataStore meant to be used as an alternative to Unity's _EditorPrefs_. It allows a custom editor to store persistent data for editors by storing instances inheriting the _PreferenceContainer_ class.

The Preference Editor _DataStore_ cannot be accessed directly, data is added using:
```c#
// Generic type T being the desired type of PreferenceContainer.
FlowEditor.PreferenceEditor.GetPreferences <T> (string id) ;
```

None of the data is actually stored until the ```Store ()``` method is called on the _PreferenceContainer_ object. To remove the data from the _DataStore_ use:
```c#
FlowEditor.PreferenceEditor.RemovePreferences (string id) ;
```


### Tools <a name="tools"></a>
Currently Tools are made by implementing the generic _EditorTool_ class, however these are planned to change when implementing a Context Aware toolbar that is meant to enforce the planned workflow.

```c#
using FlowEditor ;

public class SplineDrawTool : EditorTool <Spline> {

  public string inactiveIcon  { get ; }
  public string activeIcon    { get ; }
  public string toolTip       { get ; }

  public override void OnInspectorGUI () {
    // Works just like Editor.OnInspectorGUI
    // Override to change the inspector
  }

  public override void DrawSceneTool () {
    // Used to draw gizmos / handles in the scene view
  }

}
```

Editor tools can be used like any editor using Unity's built-in ```Editor.CreateEditor(object, Editor)``` method, The _CustomEditorAttribute_ or the generic _EditorToolGroup_. The _EditorToolGroup_ can be used by the ```Editor.CreateEditor(object, Editor)``` method, and will find all appropriate tools for the supplied generic type and display them as a selection menu similar to the one used by Unity's _Terrain_ component.


### Scheduled Features <a name="more"></a>
The following features are currently being implemented:
1. Context aware toolbar.
2. Custom settings window.
3. Graph window, to plot and visualize data.
4. Generic Scriptable object generator, with type selection window.
5. Action's using the strategy pattern, for performing scheduled tasks both in the editor and in game.
6. Better integration of _Global Variables_ with the _DataStore_ class (allow for automatic persistence of variables).
7. Object based tag/enum system.
8. Global variable Lists.
9. Global variable editor.


<!-- Sources -->
[707929ed]: https://youtu.be/raQ3iHhE_Kk?t=15m26s "Game Architecture with Scriptable Objects"
