using UnityEditor ;

namespace FlowEditor {

  public abstract class EditorTool <T> : Editor where T : class {

    public string inactiveIcon { get; set; }
    public string   activeIcon { get; set; }
    public string      toolTip { get; set; }

    public abstract void DrawSceneTool () ;

  }
}
