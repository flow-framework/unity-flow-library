using UnityEngine ;
using FlowEngine ;
using System ;

namespace FlowEditor {

  public static class AssetUtils {

    /* CreateProxy<T>
     * string name, Object Asset
     *
     * Desc.
     * Creates a GameObject with component T
     *
     * Ret. T
     */
    public static T CreateProxy<T> (string name, UnityEngine.Object asset) where T : AssetProxy {
      var gameObject  = new GameObject(name) ;
      var component   = gameObject.AddComponent<T>() ;
      component.Asset = asset ;

      return component ;
    }

    /* CreateProxy
     * string name, Object asset, Type targetComponent
     *
     * Desc.
     * Creates a GameObject with component AssetProxy from targetComponent
     *
     * Ret. AssetProxy
     */
    public static AssetProxy CreateProxy (string name, UnityEngine.Object asset, Type targetComponent) {
      if( !targetComponent.IsSubclassOf(typeof(AssetProxy)) )
        throw new Exception ("Target class (" + targetComponent.Name + ") should implement type (FlowEngine.AssetProxy)") ;

      var gameObject  = new GameObject(name);
      var component   = gameObject.AddComponent(targetComponent) as AssetProxy ;
      component.Asset = asset ;

      return component ;
    }

  }
}
